# Symulacja Wahadła Odwróconego

Projekt polega na symulacji wózka z odwróconym wahadłem. Celem jest osiągnięcie stabilnej pozycji układu stosując metody sterowania LQR (Linear Quadratic Regulator) oraz metodę Ackermana do osiągnięcia zadanej pozycji końcowej. Model uwzględnia warianty parametrów Q i R dla metody LQR, aby pokazać ich wpływ na wyniki symulacji.

## Wymagania

Aby uruchomić ten kod, potrzebujesz następujących bibliotek Pythona:

- numpy
- matplotlib
- scipy
- control


## Struktura kodu

- Definiujemy najpierw parametry systemu, takie jak masa, długość, moment bezwładności, tarcie, a także grawitację.
- Następnie definiujemy macierze modelu, które opisują dynamikę systemu.
- Określamy parametry symulacji, takie jak czas trwania, krok czasowy, warunki początkowe i końcowe.
- Kolejnym krokiem jest określenie biegunów dla metody Ackermana i obliczenie współczynników sterowania.
- W przypadku metody LQR, definiujemy różne zestawy parametrów Q i R, które wpływają na wynik sterowania.
- Po obliczeniu odpowiednich współczynników sterowania, przeprowadzamy symulację dla różnych zestawów parametrów.
- Na końcu tworzymy wykresy dla każdej symulacji, które pokazują jak różne parametry i metody wpływają na dynamikę systemu.
