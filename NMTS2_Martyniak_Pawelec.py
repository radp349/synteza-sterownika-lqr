import matplotlib
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import ss2tf
import scipy
import control
#175631, 181578
# b >= a
a=1 
b=8 

#parametry
M = 0.5+b # [kg]
m = 0.1+0.1*a # [kg]
L = 0.3 # [m]
I = 0.006 # [kg*m^2]
b = 0.1 # [N*s/m]
g = 9.80665 # [m/s^2]

mianownik=M*I+M*m*L*L+m*I

#model
A = np.array([
    [0, 1, 0, 0],
    [0, (-b * I - b * m * L * L) / mianownik, (-m * m * L * L * g) / mianownik, 0],
    [0, 0, 0, 1],
    [0, (m * L * b) / mianownik, (M + m) * m * g * L / mianownik, 0]
])
B = np.array([[0], [(I + m * L * L) / mianownik], [0], [-(m * L) / mianownik]])
C = np.array([1, 0, 0, 0])
D = np.array([0])

tmax = 40
dt = 0.001
t = np.arange(0, tmax, dt)
liczba_krokow = int(tmax / dt)
u = np.zeros(len(t))
yr = -2
Yr = np.ones(len(t)) * yr
X = np.zeros((4, len(t)))
x_0 = np.array([[-10], [0], [0.05], [0]])
x_K = np.array([[2], [0], [0], [0]])
w = [0, 0, 0, 1]


#parametry dla formuły Ackermana
Pc = np.dot(control.ctrb(A, B), np.identity(A.shape[0]))

# Bieguny (s+1)(s+2)(s+3)(s+4)
a_wsp = np.convolve(np.convolve(np.convolve([1, 1], [1, 2]), [1, 3]), [1, 4])

wielomian = np.dot(np.dot(np.dot(np.dot(a_wsp[0], A), A), A), A) + \
            np.dot(np.dot(np.dot(a_wsp[1], A), A), A) + \
            np.dot(np.dot(a_wsp[2], A), A) + \
            np.dot(a_wsp[3], A) + \
            a_wsp[4]

K_ackerman = np.dot(np.dot(w, np.linalg.inv(Pc)), wielomian)


#parametry algorytmu LQR
Q1 = np.eye(4) * 0.1
R1 = np.ones((1, 1)) * 0.1
Q2 = np.eye(4) * 0.1
R2 = np.ones((1, 1)) * 500
Q3 = np.eye(4) * 500
R3 = np.ones((1, 1)) * 0.1
# a/b mieści się w przedziale <0,0.25> więc ustawimy "Only one state parameter inaccuracy is expensive" na 𝑦(𝑡)
Q4 = np.eye(4)*0.1
Q4[0][0] = 500
R4 = np.ones((1, 1)) * 0.1



def symulacja(dt, iterations, x_0, x_ref, A, B, K):
    x = x_0
    y = [[],[],[],[]]
    for t in range(iterations):
        u = np.dot(K, (x_ref - x))
        x = x + np.dot(A, x) * dt + B * u * dt
        y[0].append(x[0, 0])
        y[1].append(x[1, 0])
        y[2].append(x[2, 0])
        y[3].append(x[3, 0])
    return y

def wmocnieniaLQR(A, B, Q, R):
    # Współczynniki K dla metody LQR
    P = scipy.linalg.solve_continuous_are(A, B, Q, R)  # Ricatti
    K_LQR = np.dot(np.dot(np.linalg.inv(R), B.transpose()), P)
    return K_LQR

Y1 = symulacja(dt, liczba_krokow, x_0, x_K, A, B, wmocnieniaLQR(A, B, Q1, R1))
Y2 = symulacja(dt, liczba_krokow, x_0, x_K, A, B, wmocnieniaLQR(A, B, Q2, R2))
Y3 = symulacja(dt, liczba_krokow, x_0, x_K, A, B, wmocnieniaLQR(A, B, Q3, R3))
Y4 = symulacja(dt, liczba_krokow, x_0, x_K, A, B, wmocnieniaLQR(A, B, Q4, R4))
Y5 = symulacja(dt, liczba_krokow, x_0, x_K, A, B, K_ackerman)

plt.figure(1)  # LQR
plt.subplot(221)
plt.plot(t, Y1[0][:], '-',label='Q = 0.1, R = 0.1', color='magenta')
plt.plot(t, Y2[0][:], '--',label='Q = 0.1, R = 500', color='orange')
plt.plot(t, Y3[0][:], label='Q = 500, R = 0.1', color='green')
plt.plot(t, Y4[0][:], label='Q = 0.1 (ale Q[0][0] = 500), R = 0.1', color='blue')
plt.legend(title='Wykresy dla różnych parametrów Q i R')
plt.title('Położenie liniowe wózka')
plt.xlabel('Czas [s]')
plt.ylabel('Położenie liniowe[m]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(222)
plt.plot(t, Y1[1][:], '-',label='Q = 0.1, R = 0.1', color='magenta')
plt.plot(t, Y2[1][:], '--',label='Q = 0.1, R = 500', color='orange')
plt.plot(t, Y3[1][:], label='Q = 500, R = 0.1', color='green')
plt.plot(t, Y4[1][:], label='Q = 0.1 (ale Q[0][0] = 500), R = 0.1', color='blue')
plt.legend(title='Wykresy dla różnych parametrów Q i R')
plt.title('Prędkość wózka')
plt.xlabel('Czas [s]')
plt.ylabel('Prędkość liniowa [m/s]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(223)
plt.plot(t, Y1[2][:], '-',label='Q = 0.1, R = 0.1', color='magenta')
plt.plot(t, Y2[2][:], '--',label='Q = 0.1, R = 500', color='orange')
plt.plot(t, Y3[2][:], label='Q = 500, R = 0.1', color='green')
plt.plot(t, Y4[2][:], label='Q = 0.1 (ale Q[0][0] = 500), R = 0.1', color='blue')
plt.legend(title='Wykresy dla różnych parametrów Q i R')
plt.title('Położenie kątowe odwróconego wahadła')
plt.xlabel('Czas [s]')
plt.ylabel('Położenie kątowe [rad]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(224)
plt.plot(t, Y1[3][:], '-',label='Q = 0.1, R = 0.1', color='magenta')
plt.plot(t, Y2[3][:], '--',label='Q = 0.1, R = 500', color='orange')
plt.plot(t, Y3[3][:], label='Q = 500, R = 0.1', color='green')
plt.plot(t, Y4[3][:], label='Q = 0.1 (ale Q[0][0] = 500), R = 0.1', color='blue')
plt.legend(title='Wykresy dla różnych parametrów Q i R')
plt.title('Prędkość kątowa odwróconego wahadła')
plt.xlabel('Czas [s]')
plt.ylabel('Prędkość kątowa [rad/s]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.figure(2)  # metoda Ackermana
plt.subplot(221)
plt.plot(t, Y5[0][:], color='purple')
plt.title('Położenie liniowe wózka')
plt.xlabel('Czas [s]')
plt.ylabel('Położenie liniowe[m]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(222)
plt.plot(t, Y5[1][:], color='orange')
plt.title('Prędkość wózka')
plt.xlabel('Czas [s]')
plt.ylabel('Prędkość liniowa [m/s]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(223)
plt.plot(t, Y5[2][:], color='green')
plt.title('Położenie kątowe odwróconego wahadła')
plt.xlabel('Czas [s]')
plt.ylabel('Położenie kątowe [rad]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(224)
plt.plot(t, Y5[3][:], color='blue')
plt.title('Prędkość kątowa odwróconego wahadła')
plt.xlabel('Czas [s]')
plt.ylabel('Prędkość kątowa [rad/s]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.figure(3)  # porownanie
plt.subplot(221)
plt.plot(t, Y1[0][:], label='Metoda LQR, praametry: Q = 0.1, R = 0.1', color='red')
plt.plot(t, Y5[0][:], label='Metoda Ackermana', color='blue')
plt.legend(title='Wykresy dla różnych metod')
plt.title('Położenie liniowe wózka')
plt.xlabel('Czas [s]')
plt.ylabel('Położenie liniowe[m]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(222)
plt.plot(t, Y1[1][:], label='Metoda LQR, praametry: Q = 0.1, R = 0.1', color='orange')
plt.plot(t, Y5[1][:], label='Metoda Ackermana', color='green')
plt.legend(title='Wykresy dla różnych metod')
plt.title('Prędkość wózka')
plt.xlabel('Czas [s]')
plt.ylabel('Prędkość liniowa [m/s]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(223)
plt.plot(t, Y1[2][:], label='Metoda LQR, praametry: Q = 0.1, R = 0.1', color='purple')
plt.plot(t, Y5[2][:], label='Metoda Ackermana', color='yellow')
plt.legend(title='Wykresy dla różnych metod')
plt.title('Położenie kątowe odwróconego wahadła')
plt.xlabel('Czas [s]')
plt.ylabel('Położenie kątowe [rad]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.subplot(224)
plt.plot(t, Y1[3][:], label='Metoda LQR, praametry: Q = 0.1, R = 0.1', color='pink')
plt.plot(t, Y5[3][:], label='Metoda Ackermana', color='cyan')
plt.legend(title='Wykresy dla różnych metod')
plt.title('Prędkość kątowa odwróconego wahadła')
plt.xlabel('Czas [s]')
plt.ylabel('Prędkość kątowa [rad/s]')
plt.grid(axis='x', color='0.95')
plt.grid(axis='y', color='0.95')

plt.show()